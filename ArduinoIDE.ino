#include <DHT.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

const char* ssid = "Nazwa WiFi";
const char* password = "Hasło WiFi";

const char* mqtt_server = "IP serwera MQTT";
const char* mqtt_user = "Login serwera MQTT";
const char* mqtt_passw = "Hasło serwera MQTT";
int mqtt_port = 1883; //Port Serwera MQTT

uint8_t DHTPin = D8;
#define DHTTYPE DHT22

DHT dht(DHTPin, DHTTYPE);

float Temperature;
float Humidity;

WiFiClient espClient;
PubSubClient client(espClient);

void setup() { 
 pinMode(BUILTIN_LED, OUTPUT);
 Serial.begin(9600);
 delay(100);
 pinMode(DHTPin, INPUT);
 dht.begin();
 
 setup_wifi();
 client.setServer(mqtt_server, mqtt_port);
 client.setCallback(callback);
 
 Serial.println("Connected ");
 Serial.print("MQTT Server ");
 Serial.print(mqtt_server);
 Serial.print(":");
 Serial.println(String(mqtt_port)); 
 Serial.print("ESP8266 IP ");
 Serial.println(WiFi.localIP()); 
 Serial.println("Modbus RTU Master Online");
 
 
}

void setup_wifi() {
 
 delay(10);
 
 Serial.println();
 Serial.print("Connecting to ");
 Serial.println(ssid);
 
 WiFi.begin(ssid, password);
 
 while (WiFi.status() != WL_CONNECTED) {
   delay(500);
   Serial.print(".");
 }
 
 Serial.println("");
 Serial.println("WiFi connected");
 Serial.println("IP address: ");
 Serial.println(WiFi.localIP());
}

void reconnect() {
 
 while (!client.connected()) {
   Serial.print("Attempting MQTT connection...");
 
   if (client.connect("ESP8266Client")) {
     Serial.println("connected");
   } 
   else {
     Serial.print("failed, rc=");
     Serial.print(client.state());
     Serial.println(" try again in 5 seconds");
     
     delay(5000);
      }
   }
}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();
 
  delay(10000);
  
  Temperature = dht.readTemperature(); 
  Humidity = dht.readHumidity();
  Serial.print("Temp: ");
  Serial.println(Temperature);
  Serial.print("Hum: ");
  Serial.println(Humidity);

  char temperaturenow [15];
  dtostrf(Temperature,7, 2, temperaturenow); 
  client.publish("Betonejro/Temp", temperaturenow); 
  

}
